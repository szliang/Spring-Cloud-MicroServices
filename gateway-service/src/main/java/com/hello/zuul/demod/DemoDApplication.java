package com.hello.zuul.demod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;

@EnableZuulProxy
@EnableDiscoveryClient
@SpringBootApplication
@EnableRedisHttpSession
public class DemoDApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoDApplication.class, args);
    }

    @Configuration
    protected class HttpSessionConfig {
        @Bean
        public HttpSessionStrategy httpSessionStrategy() {
            return new HeaderHttpSessionStrategy();
        }
    }
}
