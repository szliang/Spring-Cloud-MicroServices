package com.hello.demoeurkeaclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RefreshScope
public class DemoController {

    @Value("${work.version}")
    private String version;

    @RequestMapping("/demo")
    public String go(HttpServletRequest request) {
        if (request.getHeader("Authorization") != null) {
            return "I am demo eureka client service A" + version + "===" + request.getHeader("Authorization");
        } else {
            return "Auth is null";
        }
    }
}
