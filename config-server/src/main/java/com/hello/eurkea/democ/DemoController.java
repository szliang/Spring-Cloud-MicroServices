package com.hello.eurkea.democ;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @RequestMapping("/demo-c")
    public String go(){
        return "I am demo c";
    }
}
