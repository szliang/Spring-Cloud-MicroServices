package com.hello.eurkea.democ;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class DemoCApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoCApplication.class, args);
	}
}
