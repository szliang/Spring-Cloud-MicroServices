package com.hello.demoeurkeaclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class DemoEurkeaClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoEurkeaClientApplication.class, args);
	}
}
