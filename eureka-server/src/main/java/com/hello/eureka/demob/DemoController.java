package com.hello.eureka.demob;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @RequestMapping(path = "/demo-b")
    public String go(){
        return "I am demo b";
    }
}
